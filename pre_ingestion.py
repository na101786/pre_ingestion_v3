import sys, traceback, argparse, textwrap

# try:
#from scripts_utils.cad_arq_tabelas import CadArqTabelas
from scripts_cadastro.cad_arq_tabelas import CadArqTabelas
from scripts_utils.utils import import_content, import_content_file, create_log, create_log_file, move_processed
from scripts_gera_ddl.gera_ddl import gera_ddl_jdbc, gera_ddl_file
from scripts_gera_dag.gera_dag import create_dag, create_dag_file
# except ModuleNotFoundError:
#     print("\n\nERROR: Sintaxe invalid: ")
#     print("Use spark-submit pre_ingestion.py XXXXXXXX for executing...\n\n")
#     sys.exit(-1)


def main():
    parser = argparse.ArgumentParser(
        description=textwrap.dedent('''
                Documentar o componente aqui.
                
                Para usar o componente para arquivo é preciso ter criado um csv, no s3 storage:
                s3://natura-datalake-{env}/pasta_no_s3/arquivo_metadados.csv
                Para mais detalhes, veja um exemplo em:
                s3://natura-datalake-prd/file_metadados/AR_ITEM_PEDIDO.csv
                
                How to use:
                1) spark-submit /home/hadoop/componente_pre_ingestion/pre_ingestion.py -env prd -type jdbc -layer all

                '''),
        formatter_class=argparse.RawTextHelpFormatter
    )
    # Get the params
    parser.add_argument('-env', '--env', nargs='?', metavar='Environment', type=str,
                        required=True,
                        help='Environment to run',
                        choices=['dev', 'prd'],
                        default="prd")
    parser.add_argument('-type', '--type', nargs='?', metavar='jdbc or file', type=str,
                        help='Execution to jdbc or file',
                        required=True,
                        choices=['jdbc', 'file'],
                        default='jdbc')
    parser.add_argument('-layer', '--layer', nargs='?', metavar='layer', type=str,
                        help='layer',
                        required=True,
                        choices=['raw', 'trusted', 'all'],
                        default='all'
                        )
    args = parser.parse_args()

    env = args.env
    source_type = args.type
    layer = args.layer

    # Ordem de execução (escolhido com base na probabilidade de erro):
    # 1 - DLL e tabela hive
    # 2 - Gera dag
    # 3 - Cadastro no cad_tabelas
    # 4 - Cadastro no cad_arq

    if source_type == 'jdbc':
        # instaciando CadTabelas e CadArq
        CadTabelas = CadArqTabelas(env, "repo.cad_tabelas",
        "REPOSITORIOINGESTION", "postgresql")
        CadArq = CadArqTabelas(env, "repo.cad_arq", 
        "REPOSITORIOINGESTION","postgresql")

        df_ok = import_content(env)
        nlines = df_ok.shape[0]

        for i in range(nlines):
            #count_id = df_ok.loc[i]['COD']
            #volumetry = df_ok.loc[i]['Volumetry']
            dbname = df_ok.loc[i]['dbname']
            nome_arq = df_ok.loc[i]['nome_arq']
            periodicidade = df_ok.loc[i]['periodicidade']
            destino = df_ok.loc[i]['destino']
            database = df_ok.loc[i]['database']
            tabela = df_ok.loc[i]['tabela']
            incremtal_overwrite = df_ok.loc[i]['incremtal_overwrite']
            servidor = df_ok.loc[i]['servidor']
            schema = df_ok.loc[i]['schema']
            #where = df_ok.loc[i]['where']
            where = 'null'
            #driver = df_ok.loc[i]['driver']
            driver = 'null'
            split_by = df_ok.loc[i]['split-by']
            mappers = df_ok.loc[i]['mappers']
            fetch_size = df_ok.loc[i]['fetch-size']
            id_banco = df_ok.loc[i]['id_banco']
            cast = df_ok.loc[i]['cast']
            delta_column = df_ok.loc[i]['delta_column1']
            print('DELTA_COLUMN ->', delta_column)
            #ignore = df_ok.loc[i]['ignore']
            ignore = 'null'
            print(f'IGNORE -> {ignore}')
            file_path = df_ok.loc[i]['file_path']
            delimiter = df_ok.loc[i]['delimiter']
            crontab = df_ok.loc[i]['crontab']
            crontab = crontab.replace('"', "").replace("'", "")
            owner = df_ok.loc[i]['owner']
            owner = owner.replace('"', "").replace("'", "")
            partition = df_ok.loc[i]['partitioned'].lower()
            delta_column2 = df_ok.loc[i]['delta_column2'].lower()
            delta_operation = df_ok.loc[i]['delta_operation'].lower()
            email_on_failure = df_ok.loc[i]['email_on_failure'].lower()
            try:
                # Cria tabela Hive na camada raw, trusted ou em ambas
                gera_ddl_jdbc(dbname=dbname, servidor=servidor, schema=schema, tabela=tabela, env=env, layer=layer, partition=partition)  # gera dll de banco para tabela hive

                # Cria uma Airflow DAG
                create_dag(servidor=servidor, schema=schema, tabela=tabela, crontab=crontab, owner=owner, env=env, partition=partition, email_on_failure=email_on_failure)

                # Cadastra no cad_tabelas
                CadTabelas.insert_cad_tabelas(servidor, schema, tabela, where,
                driver, split_by, mappers, fetch_size, id_banco, cast, ignore,
                delta_column, delta_column2, delta_operation)
                
                # Cadastra no cad_arq
                CadArq.insert_cad_arq(nome_arq, periodicidade, destino, database,
                tabela, incremtal_overwrite)

                # Cria log de cadastro
                create_log(df_ok.loc[i], env) # Salva o resgistro no arquivo LOG.csv localizado no s3://
                print("##########################################")
                print("STATUS REGISTRO DA TABELA: SUCCESSFULLY!")
                print("##########################################")
            except Exception:
                print("#########################################")
                print("STATUS REGISTRO DA TABELA: ERROR!")
                print("##########################################")
                traceback.print_exc()
                sys.exit(-1)

    elif source_type == 'file':
        CadArq = CadArqTabelas(env, "repo.cad_arq", 
        "REPOSITORIOINGESTION","postgresql")
        df_ok = import_content_file(env)
        nlines = df_ok.shape[0]

        for i in range(nlines):

            # Dados para gera dll
            file_path = f"s3://natura-datalake-{env}/file_metadados/"
            table = df_ok.loc[i]['file'].upper()
            delimiter = df_ok.loc[i]['delimiter'] #hive
            if 't' in delimiter:
                delimiter = '\t'
            #ref_dtype = 'oracle'
            try:
                ref_dtype = df_ok.loc[i]['ref_dtype']
                ref_dtype = ref_dtype.lower() if ref_dtype != "None" else eval(ref_dtype)
            except KeyError:
                ref_dtype = 'oracle'

            # Dados para gera dag
            file_name = df_ok.loc[i]['file']
            file_mask = df_ok.loc[i]['file_mask']
            if file_mask and file_mask != 'None':
                file_name = file_mask
            extensao = df_ok.loc[i]['extension']
            file_name = file_name + '.' + extensao
            crontab = df_ok.loc[i]['crontab']
            crontab = crontab.replace('"',"").replace("'","")
            user = df_ok.loc[i]['owner']
            user = user.replace('"', "").replace("'", "")
            source = df_ok.loc[i]['source'].lower()
            path = df_ok.loc[i]['path']
            file_type = df_ok.loc[i]['file_type'].lower()
            bucket_owner = df_ok.loc[i]['bucket_owner'].capitalize()
            has_date_in = df_ok.loc[i]['has_date_in']
            has_date_in = has_date_in.lower() if has_date_in != "None" else has_date_in
            column_name = df_ok.loc[i]['column_name']
            date_format = df_ok.loc[i]['date_format'].lower()
            has_count_in = df_ok.loc[i]['has_count_in']
            has_count_in = has_count_in.lower() if has_count_in != "None" else has_count_in
            count_position = df_ok.loc[i]['count_position']

            # Dados para cad_arq - table já foi lido
            nome_arq = 'file.' + df_ok.loc[i]['file'].lower()
            database = df_ok.loc[i]['database']
            periodicidade = df_ok.loc[i]['periodicidade']
            destino = df_ok.loc[i]['destino']
            incremtal_overwrite = df_ok.loc[i]['incremtal_overwrite']

            try:
                # Cria tabela Hive na camada raw, trusted ou em ambas
                gera_ddl_file(tabela=table, file_path=file_path, delimiter=delimiter, env=env, layer=layer, ref_dtype=ref_dtype)

                # Cria uma Airflow DAG
                create_dag_file(crontab, env, source,  path, file_name, file_type, user, table,
                                bucket_owner, has_date_in, column_name, date_format, has_count_in, count_position)

                # Cadastra no cad_arq
                CadArq.insert_cad_arq(nome_arq, periodicidade, destino, database,
                tabela, incremtal_overwrite)
                
                # Cria log de cadastro
                create_log_file(df_ok.loc[i], env) # Salva o resgistro no arquivo LOG.csv localizado no s3://
                print("##########################################")
                print("STATUS REGISTRO DA TABELA: SUCCESSFULLY!")
                print("##########################################")
            except Exception:
                print("#########################################")
                print("STATUS REGISTRO DA TABELA: ERROR!")
                print("##########################################")
                traceback.print_exc()
                sys.exit(-1)
    else:
        print("ERROR: You must choose tag -type as jdbc or file. The program will be closed.")

    # Move os arquivos já processados
    #move_processed(env)

#            gera_ddl_file(tabela, file_path, delimiter, env, layer)  # gera dll de arquivo para tabela hive


if __name__ == '__main__':
    main()
