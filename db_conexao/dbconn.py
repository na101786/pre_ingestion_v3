import s3fs, cx_Oracle, psycopg2, mysql.connector
from hdbcli import dbapi
from mysql.connector import errorcode


class dbConn(object):
    def __init__(self, env: str, dbname: str,  database: str):
        self.env = env.lower()
        self.dbname = dbname.capitalize()
        self.database = database.upper()

    def get_connection_params(self) -> tuple:
        s3 = s3fs.S3FileSystem()
        credentials_file = s3.open(f"s3://natura-datalake-{self.env}/sqoop/SqoopConnect{self.dbname}{self.database}.txt", mode="r")
        linhas = credentials_file.readlines()
        credentials_file.close()

        connect = host = port = bd = username = password = v_driver = jdbc_url = aws_iam_role = ""

        for i, linha in enumerate(linhas):
            if '--connect' in linha:
                connect = linhas[i + 1][:-1]
                connect = connect.strip()
                _connect = connect.split('//')[1]
                host = _connect.split(':')[0]
                port = _connect.split(':')[1].split('/')[0]
                bd = _connect.split('/')[-1]
            if '--username' in linha:
                username = linhas[i + 1][:-1]
                username = username.strip()
            if '--password' in linha:
                password = linhas[i + 1][:-1]
                password = password.strip()
            if '--driver' in linha:
                v_driver = linhas[i + 1][:-1]
                v_driver = v_driver.strip()
            if '--aws_iam_role' in linha:
                aws_iam_role = linhas[i + 1][:-1]
                aws_iam_role = aws_iam_role.strip()

        if self.dbname.lower() == 'redshift':
            jdbc_url = f"jdbc:redshift://{host}:{port}/{bd}?user={username}&password={password}"
        elif self.dbname.lower() == 'oracle':
            jdbc_url = connect

        params = {
            'host': host,
            'port': port,
            'database': bd,
            'username': username,
            'password': password,
            'v_driver': v_driver,
            'jdbc_url': jdbc_url,
            'aws_iam_role': aws_iam_role
        }
        return params

    def get_s3_jdbc_connection_params(self) -> tuple:
        self.dbname = self.dbname.upper()
        s3 = s3fs.S3FileSystem()
        credentials_file = s3.open(f"s3://natura-datalake-{self.env}/sqoop/jdbc/{self.dbname}.txt", mode="r")
        linhas = credentials_file.readlines()
        credentials_file.close()

        connect = host = port = bd = username = password = v_driver = jdbc_url = aws_iam_role = ""

        for i, linha in enumerate(linhas):
            if '--connect' in linha:
                connect = linhas[i + 1][:-1]
                connect = connect.strip()
                _connect = connect.split('//')[1]
                host = _connect.split(':')[0]
                port = _connect.split(':')[1].split('/')[0]
                bd = _connect.split('/')[-1]
            if '--username' in linha:
                username = linhas[i + 1][:-1]
                username = username.strip()
            if '--password' in linha:
                password = linhas[i + 1][:-1]
                password = password.strip()
            if '--driver' in linha:
                v_driver = linhas[i + 1][:-1]
                v_driver = v_driver.strip()
            if '--aws_iam_role' in linha:
                aws_iam_role = linhas[i + 1][:-1]
                aws_iam_role = aws_iam_role.strip()

        jdbc_url = connect

        params = {
            'host': host,
            'port': port,
            'database': bd,
            'username': username,
            'password': password,
            'v_driver': v_driver,
            'jdbc_url': jdbc_url,
            'aws_iam_role': aws_iam_role
        }
        return params    

    def get_sqlserver_connection_params(self) -> tuple:
        s3 = s3fs.S3FileSystem()
        credentials_file = s3.open(
            f"s3://natura-datalake-{self.env}/sqoop/SqoopConnect{self.dbname}{self.database}.txt", mode="r")
        linhas = credentials_file.readlines()
        credentials_file.close()
        for i, linha in enumerate(linhas):
            if '--connect' in linha:
                jdbc_url = linhas[i + 1][:-1]
                jdbc_url = jdbc_url.strip()
            if '--username' in linha:
                username = linhas[i + 1][:-1]
                username = username.strip()
            if '--password' in linha:
                password = linhas[i + 1][:-1]
                password = password.strip()

        params = {
            'username': username,
            'password': password,
            'jdbc_url': jdbc_url,
          }
        return params

    def connect_oracle(self) -> cx_Oracle.connect:
        params = self.get_connection_params()
        host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        conn_str = fr"{username}/{password}@{host}:{port}/{database}"
        return cx_Oracle.connect(conn_str)

    def connect_postgresql(self) -> psycopg2.connect:
        params = self.get_connection_params()
        host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        conn_str = fr"dbname='{database}' user='{username}' host='{host}' password='{password}' port='{port}'"
        return psycopg2.connect(conn_str)

    def connect_redshift(self) -> psycopg2.connect:
        params = self.get_connection_params()
        host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        conn_str = fr"dbname='{database}' user='{username}' host='{host}' password='{password}' port='{port}'"
        return psycopg2.connect(conn_str)

    def connect_sqlserver(self):
        pass

    def connect_saphana(self) -> dbapi.connect:
        # params = self.get_connection_params()
        # host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        # conn_str = fr"dbname='{database}' user='{username}' host='{host}' password='{password}' port='{port}'"
        # return psycopg2.connect(conn_str)
        params = self.get_connection_params()
        host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        return dbapi.connect(
            address=host,
            port=port,
            user=username,
            password=password
        )
    
    def connect_mysql(self) -> mysql.connector:
        params = self.get_connection_params()
        host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        #conn_str = fr"dbname='{database}' user='{username}' host='{host}' password='{password}' port='{port}'"
        try:
            cnn = mysql.connector.connect(user=username, password=password, host=host, database=database)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        return cnn

    def connect_cad_arq_tabelas(self) -> psycopg2.connect:
        params = self.get_s3_jdbc_connection_params()
        host, port, database, username, password = params['host'], params['port'], params['database'], params['username'], params['password']
        conn_str = fr"dbname='{database}' user='{username}' host='{host}' password='{password}' port='{port}'"
        return psycopg2.connect(conn_str)
