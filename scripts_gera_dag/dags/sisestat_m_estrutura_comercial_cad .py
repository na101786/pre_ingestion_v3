from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator

batch = (datetime.today() - timedelta(days=1)).strftime('%Y%m%d')

default_args = {
   'owner': "migracao_azure",
   'depends_on_past': False,
   'start_date': datetime(2019, 1, 1),
   'retries': 3,
   }

cmd_template = """ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.{{ params.env }}.naturacloud.com \
spark-submit \
/home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py \
-env {{ params.env }} \
-system_table {{ params.system_table }} \
-source_type jdbc \
-partition {{ params.batch }} \
-user {{ params.owner  }} \
--{{ params.stage }}
"""

with DAG(
   'sisestat_m_estrutura_comercial_cad ',
   schedule_interval="43 03 * * *",
   catchup=False,
   default_args=default_args
   ) as dag1:
    t1 = BashOperator(
        task_id='carga_origem_landing',
        bash_command=cmd_template,
        params={
                    'env': 'prd',
                    'system_table': 'sisestat.m_estrutura_comercial_cad ',
                    'batch': batch,
                    'owner': 'migracao_azure',
                    'stage': 'jdbc_to_landing'
                 }
        )
    t2 = BashOperator(
        task_id='landing_to_raw',
        bash_command=cmd_template,
        params={
                    'env': 'prd',
                    'system_table': 'sisestat.m_estrutura_comercial_cad ',
                    'batch': batch,
                    'owner': 'migracao_azure',
                    'stage': 'landing_to_raw'
                     }
            )
    t3 = BashOperator(
        task_id='raw_to_trusted',
        bash_command=cmd_template,
        params={
                    'env': 'prd',
                    'system_table': 'sisestat.m_estrutura_comercial_cad ',
                    'batch': batch,
                    'owner': 'migracao_azure',
                    'stage': 'raw_to_trusted'
                 }
        )

# Definindo o padrao de execucao, nesse caso executamos t1 e depois t2
t1 >> t2 >> t3
