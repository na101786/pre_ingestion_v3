from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.bash_operator import BashOperator
from datetime import date, timedelta

batch_date = datetime.today() - timedelta(days=1)
year = str(batch_date.year)
month = str(batch_date.month)
day = str(batch_date.day)

file_ok = 'AR_ABRANGENCIA_ddmmyyyy.TXT'
file_ok = file_ok.replace('dd', f'{day}').replace('mm', month).replace('yyyy', year)

# Definindo alguns argumentos basicos
default_args = {
   'owner': 'van',
   'depends_on_past': False,
   'start_date': datetime(2019, 1, 1),
   'retries': 3,
   }

cmd1 = f"""ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.dev.naturacloud.com \
spark-submit /home/hadoop/componente_load_file/load_file.py \
-env dev \
-source server \
-bucket_owner None \
-path aslx224br://etl/dmlatam/ar/SrcFiles/ \
-file {file_ok} \
-file_type delimited \
-has_date_in file_name \
-column_name None \
-date_format ddmmyyyy \
-has_count_in header \
-count_position 4 \
-user van 2>&1"""

cmd2="""ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.dev.naturacloud.com /home/hadoop/landing_to_raw.sh file.ar_abrangencia 2>&1"""
cmd3="""ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.dev.naturacloud.com /home/hadoop/raw_to_trusted.sh file.ar_abrangencia  2>&1"""

# Nomeando a DAG e definindo quando ela vai ser executada (voce pode usar argumentos em Crontab tambem caso queira que a DAG execute por exemplo todos os dias as 8 da manha)
with DAG(
   'file_ar_abrangencia',
   schedule_interval="45 03 * * *",
   catchup=False,
   default_args=default_args
   ) as dag1:
# Definindo as tarefas que a DAG vai executar, nesse caso a execucao de dois programas Python, chamando sua execucao por comandos bash
    t1 = BashOperator(
    task_id='carga_origem_landing_file',
    bash_command=cmd1)
    t2 = BashOperator(
    task_id='landing_to_raw',
    bash_command=cmd2)

# Definindo as tarefas que a DAG vai executar, nesse caso a execucao de dois programas Python, chamando sua execucao por comandos bash
    t3 = BashOperator(
    task_id='raw_to_trusted',
    bash_command=cmd3)
# Definindo o padrao de execucao, nesse caso executamos t1 e depois t2
t1 >> t2 >> t3
