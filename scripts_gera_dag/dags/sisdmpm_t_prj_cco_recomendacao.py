from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator


batch = (datetime.today() - timedelta(days=1)).strftime('%Y%m%d')

default_args = {
   'owner': " cco",
   'depends_on_past': False,
   'start_date': datetime(2019, 1, 1),
   'retries': 3,
   }

cmd_template = """ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.{{ params.env }}.naturacloud.com \
spark-submit \
--py-files /home/hadoop/engine_ingestion_lake_layers/modulos.zip \
--deploy-mode cluster \
--driver-memory 10g \
--num-executors 6 \
--executor-cores 4 \
--executor-memory 4g \
--conf spark.dynamicAllocation.maxExecutors=6 \
/home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py \
-env {{ params.env }} \
-system_table {{ params.system_table }} \
-source_type jdbc \
-partition None \
-user {{ params.owner }} \
--{{ params.stage }}
"""

with DAG(
   'sisdmpm_t_prj_cco_recomendacao',
   schedule_interval="00 00 * * *",
   catchup=False,
   default_args=default_args
   ) as dag1:
    t1 = BashOperator(
        task_id='carga_origem_landing',
        bash_command=cmd_template,
        params={
                    'env': 'dev',
                    'system_table': 'sisdmpm.t_prj_cco_recomendacao',
                    'batch': batch,
                    'owner': ' cco',
                    'stage': 'jdbc_to_landing'
                 }
        )
    t2 = BashOperator(
        task_id='landing_to_raw',
        bash_command=cmd_template,
        params={
                    'env': 'dev',
                    'system_table': 'sisdmpm.t_prj_cco_recomendacao',
                    'batch': batch,
                    'owner': ' cco',
                    'stage': 'landing_to_raw'
                     }
            )
    t3 = BashOperator(
        task_id='raw_to_trusted',
        bash_command=cmd_template,
        params={
                    'env': 'dev',
                    'system_table': 'sisdmpm.t_prj_cco_recomendacao',
                    'batch': batch,
                    'owner': ' cco',
                    'stage': 'raw_to_bau'
                 }
        )

# Definindo o padrao de execucao, nesse caso executamos t1 e depois t2
t1 >> t2 >> t3
