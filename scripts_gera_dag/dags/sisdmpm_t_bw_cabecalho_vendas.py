from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.bash_operator import BashOperator
# Definindo alguns argumentos basicos
default_args = {
   'owner': " migracao_azure",
   'depends_on_past': False,
   'start_date': datetime(2019, 1, 1),
   'retries': 3,
   }
cmd1="""ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.dev.naturacloud.com /home/hadoop/carga_origem_landing.sh sisdmpm.t_bw_cabecalho_vendas  2>&1"""
cmd2="""ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.dev.naturacloud.com /home/hadoop/landing_to_raw.sh sisdmpm.t_bw_cabecalho_vendas 2>&1"""
cmd3="""ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.dev.naturacloud.com /home/hadoop/raw_to_bau.sh sisdmpm.t_bw_cabecalho_vendas  2>&1"""

# Nomeando a DAG e definindo quando ela vai ser executada (voce pode usar argumentos em Crontab tambem caso queira que a DAG execute por exemplo todos os dias as 8 da manha)
with DAG(
   'sisdmpm_t_bw_cabecalho_vendas',
   schedule_interval="20 04 * * *",
   catchup=False,
   default_args=default_args
   ) as dag1:
# Definindo as tarefas que a DAG vai executar, nesse caso a execucao de dois programas Python, chamando sua execucao por comandos bash
    t1 = BashOperator(
    task_id='carga_origem_landing',
    bash_command=cmd1)
    t2 = BashOperator(
    task_id='landing_to_raw',
    bash_command=cmd2)

# Definindo as tarefas que a DAG vai executar, nesse caso a execucao de dois programas Python, chamando sua execucao por comandos bash
    t3 = BashOperator(
    task_id='raw_to_bau',
    bash_command=cmd3)
# Definindo o padrao de execucao, nesse caso executamos t1 e depois t2
t1 >> t2 >> t3
