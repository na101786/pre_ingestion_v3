from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator

batch = (datetime.today() - timedelta(days=1)).strftime('%Y%m%d')

default_args = {
   'owner': "comitecrise",
   'depends_on_past': False,
   'start_date': datetime(2019, 1, 1),
   'retries': 3,
   }

cmd_template = """ssh -o "StrictHostKeyChecking=no" hadoop@coedados-emr.{{ params.env }}.naturacloud.com \
spark-submit \
--py-files /home/hadoop/engine_ingestion_lake_layers/modulos.zip \
--deploy-mode cluster \
--driver-memory 6g \
--num-executors 4 \
--executor-cores 2 \
--executor-memory 2g \
--conf spark.dynamicAllocation.maxExecutors=4 \
/home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py \
-env {{ params.env }} \
-system_table {{ params.system_table }} \
-source_type jdbc \
-partition {{ params.batch }} \
-user {{ params.owner }} \
--{{ params.stage }}
"""

with DAG(
   'sisdmi_t_prj_2go_f_pedido_item',
   schedule_interval="45 06 * * *",
   catchup=False,
   default_args=default_args
   ) as dag1:
    t1 = BashOperator(
        task_id='carga_origem_landing',
        bash_command=cmd_template,
        params={
                    'env': 'prd',
                    'system_table': 'sisdmi.t_prj_2go_f_pedido_item',
                    'batch': batch,
                    'owner': 'comitecrise',
                    'stage': 'jdbc_to_landing'
                 }
        )
    t2 = BashOperator(
        task_id='landing_to_raw',
        bash_command=cmd_template,
        params={
                    'env': 'prd',
                    'system_table': 'sisdmi.t_prj_2go_f_pedido_item',
                    'batch': batch,
                    'owner': 'comitecrise',
                    'stage': 'landing_to_raw'
                     }
            )
    t3 = BashOperator(
        task_id='raw_to_trusted',
        bash_command=cmd_template,
        params={
                    'env': 'prd',
                    'system_table': 'sisdmi.t_prj_2go_f_pedido_item',
                    'batch': batch,
                    'owner': 'comitecrise',
                    'stage': 'raw_to_bau'
                 }
        )

# Definindo o padrao de execucao, nesse caso executamos t1 e depois t2
t1 >> t2 >> t3
