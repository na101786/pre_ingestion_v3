# program gen_dag
# python gera_ddl.py schema tabela cron owner
from string import Template
# from subprocess import call, run

#    'dagrun_timeout': timedelta(hours=10)

servidor = "O01DW"
schema = "teste"
tabela = "teste"
crontab = "30 06 * * *"
owner = "comitecrise"
env = "prd"
database = servidor
partition = 'nao'

if schema:
        v_dag_name = f"{schema}_{tabela}"  # v_schema + '_' + v_table
        v_prog_name = f"{schema}.{tabela}"  # v_schema + '.' + v_table
else:
    v_dag_name = f"{database}_{tabela}"  # v_schema + '_' + v_table
    v_prog_name = f"{database}.{tabela}"  # v_schema + '.' + v_table

# if schema.lower() in ['sisdmpm']:
if database.lower() == 'o01dw':
    filein = open('./scripts_gera_dag/template_dag_bau')
else:
    filein = open('./scripts_gera_dag/template_dag')
template = Template(filein.read())
dag_final = template.substitute(env=env, dag_name=v_dag_name, prog_name=v_prog_name, crontab=crontab, owner=owner)
if partition == 'nao':
    dag_final = dag_final.replace("-partition {{ ds_nodash }} \\", "-partition None \\")
file = open(f"./scripts_gera_dag/dags/{v_dag_name}.py", 'w')
file.write(dag_final)
file.close()

"""
header = "servidor|schema|tabela|where|driver|split-by|mappers|fetch-size|id_banco|cast|ignore|delta_column"
len(header.split("|")) # 12
teste = "O01DW|SISDMI|T_PRJ_2GO_F_PEDIDO|dt_ultima_atualizacao>=trunc(sysdate-1)and(dt_ultima_atualizacao<=trunc(sysdate))|||1||1|||dt_ultima_atualizacao"
teste = "O01DW|SISDMI|T_PRJ_2GO_F_PEDIDO_ITEM|dt_ultima_atualizacao>=trunc(sysdate-1)and(dt_ultima_atualizacao<=trunc(sysdate))|||1||1|||dt_ultima_atualizacao"
len(teste.split("|"))
"""