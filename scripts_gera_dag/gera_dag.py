#program gen_dag
#python gera_ddl.py schema tabela cron owner
from string import Template
#from subprocess import call, run

import s3fs
import boto3


def create_dag(servidor, schema, tabela, crontab, owner, env: str, partition, email_on_failure):
    """
    Cria uma Airflow_DAG para execucao orquestrada e salva no s3

    Parameters
    ----------
    schema : str
        Schema da tabela na origem
    tabela : str
        Tabela de origem
    crontab : str
        Horario a ser rodada a DAG (Horario UTC)
    owner : str
        Responsavel pela DAG
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        Arquivo python (DAG) salvo no s3

    """
    database = servidor
    client = boto3.resource("s3")
    print("\n\n\n############################")
    print("EXECUTING FUNCTION CREATE_DAG")
    print("############################\n\n\n")
    if schema:
        v_dag_name = f"{schema}_{tabela}"  # v_schema + '_' + v_table
        v_prog_name = f"{schema}.{tabela}"  # v_schema + '.' + v_table
    else:
        v_dag_name = f"{database}_{tabela}"  # v_schema + '_' + v_table
        v_prog_name = f"{database}.{tabela}"  # v_schema + '.' + v_table
    
    #if schema.lower() in ['sisdmpm']:
    if database.lower() in ['o01dw', 'o01dwh']:
        filein = open('/home/hadoop/componente_pre_ingestion/scripts_gera_dag/template_dag_bau')
    else:   
        filein = open('/home/hadoop/componente_pre_ingestion/scripts_gera_dag/template_dag')
    template = Template(filein.read())
    dag_final = template.substitute(env=env, dag_name=v_dag_name, prog_name=v_prog_name, crontab=crontab, owner=owner, email_on_failure=email_on_failure)

    if partition == 'no':
        dag_final = dag_final.replace("-partition {{ ds_nodash }} \\", "-partition None \\")
    # file = open(f"/home/hadoop/componente_pre_ingestion/scripts_gera_dag/dags/{v_dag_name}.py", 'w')
    # file.write(dag_final)
    # file.close()

    # s3 = s3fs.S3FileSystem()
    # local_path = f"/home/hadoop/componente_pre_ingestion/scripts_gera_dag/dags/{v_dag_name}.py"
    # s3_path = f"s3://natura-datalake-{env}/pre_ingestion/airflow/dags/{v_dag_name}.py"
    # s3.put(local_path, s3_path)
    # if env == 'dev':
    #     s3.put(local_path, f"s3://s3-natura-us-east-1-dev-airflow-bucket-dags/{v_dag_name}.py")

    # Inserido essa parte de baixo devido a permission denied na pasta hadoop
    
    client.Bucket(f"natura-datalake-{env}").put_object(
        Body=dag_final, Key=f'pre_ingestion/airflow/dags/{v_dag_name}.py')
    if env == 'dev':
        client.Bucket(f"s3-natura-us-east-1-{env}-airflow-bucket-dags").put_object(
            Body=dag_final,Key=f"{v_dag_name}.py")
    
    
#    call(['chmod', '770', local_path])    
    #run(["scp", local_path, f"ubuntu@airflow.{env}.naturacloud.com:/usr/local/airflow/dags/"])


def create_dag_file(crontab,
                    env,
                    source,
                    path,
                    file_name,
                    file_type,
                    user,
                    table,
                    bucket_owner='None',
                    has_date_in='None',
                    column_name='None',
                    date_format='None',
                    has_count_in='None',
                    count_position='None'):

    print("\n\n\n############################")
    print("EXECUTING FUNCTION CREATE_DAG")
    v_dag_name = f"file_{table.lower()}"
    v_prog_name = f"file.{table.lower()}"

    filein = open('/home/hadoop/componente_pre_ingestion/scripts_gera_dag/template_dag_file')
    template = Template(filein.read())
    dag_final = template.substitute(dag_name=v_dag_name,
                                    prog_name=v_prog_name,
                                    crontab=crontab,
                                    env=env,
                                    source=source,
                                    path=path,
                                    file_name=file_name,
                                    file_type=file_type,
                                    user=user,
                                    bucket_owner=bucket_owner,
                                    has_date_in=has_date_in,
                                    column_name=column_name,
                                    date_format=date_format,
                                    has_count_in=has_count_in,
                                    count_position=count_position
                                    )
    # with open(f"/home/hadoop/componente_pre_ingestion/scripts_gera_dag/dags/{v_dag_name}.py", 'w') as file_w:
    #     file_w.write(dag_final)

    # s3 = s3fs.S3FileSystem()
    # local_path = f"/home/hadoop/componente_pre_ingestion/scripts_gera_dag/dags/{v_dag_name}.py"
    # s3_path = f"s3://natura-datalake-{env}/pre_ingestion/airflow/dags/{v_dag_name}.py"
    # s3.put(local_path, s3_path)

    # Inserido essa parte de baixo devido a permission denied na pasta hadoop
    client = boto3.resource("s3")
    client.Bucket(f"natura-datalake-{env}").put_object(
        Body=dag_final, Key=f'pre_ingestion/airflow/dags/{v_dag_name}.py')
    if env == 'dev':
        client.Bucket(f"s3-natura-us-east-1-{env}-airflow-bucket-dags").put_object(
            Body=dag_final,Key=f"{v_dag_name}.py")

    #    call(['chmod', '770', local_path])
    #run(["scp", local_path, f"ubuntu@10.188.1.9:/home/ubuntu/airflow/dags"])
    print("CREATING DAG OK..")
    print("############################\n\n\n")
