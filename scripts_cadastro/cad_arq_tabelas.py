import pandas as pd
import os, sys
import psycopg2
from pyspark.sql import SparkSession
from scripts_utils.dbsecrets import get_autentication
from db_conexao.dbconn import dbConn

class CadArqTabelas(object):

    def __init__(self, env: str, dbtable: str, dbname: str, db: str):
        self.env = env
        self.dbtable = dbtable
        self.dbname = dbname
        self.db = db

    def get_cad_tabelas_as_df(self):
        """
        Devolve um DF da tabela repo.cad_tabelas
        """
        spark = SparkSession.builder.getOrCreate()
        spark.sparkContext.setLogLevel('ERROR')

        paramns = dbConn(env=self.env, dbname=self.dbname, database=self.db).get_s3_jdbc_connection_params()
        jdbc_url = paramns['jdbc_url']
        #user = paramns['username']
        #password = paramns['password']
        user = get_autentication("coedados/rds/repositorioingestion/username")['SecretString']
        password = get_autentication("coedados/rds/repositorioingestion/password")['SecretString']
        driver = paramns['v_driver']

        df = spark.read.format("jdbc") \
            .option("url", jdbc_url) \
            .option("driver", driver) \
            .option("dbtable", self.dbtable) \
            .option("user", user) \
            .option("password", password).load()
            #.option("query", query).load()

        if df.count() == 0:
            print("\n\n\n############################")
            print("ERROR: Database, or table, or schema not found in query:\n")
            print(self.dbtable)
            print("The program will be end.")
            sys.exit(-1)
            print("############################\n\n\n")

        df_pandas = df.toPandas()
        print('CADTABELAS', df_pandas)
        return df_pandas

    def get_cad_arq_as_df(self):
        """
        Devolve um DF da tabela repo.cad_arq
        """
        spark = SparkSession.builder.getOrCreate()
        spark.sparkContext.setLogLevel('ERROR')

        paramns = dbConn(env=self.env, dbname=self.dbname, database=self.db).get_s3_jdbc_connection_params()
        jdbc_url = paramns['jdbc_url']
        #user = paramns['username']
        #password = paramns['password']
        user = get_autentication("coedados/rds/repositorioingestion/username")['SecretString']
        password = get_autentication("coedados/rds/repositorioingestion/password")['SecretString']
        driver = paramns['v_driver']

        df = spark.read.format("jdbc") \
            .option("url", jdbc_url) \
            .option("driver", driver) \
            .option("dbtable", self.dbtable) \
            .option("user", user) \
            .option("password", password).load()
            #.option("query", query).load()

        if df.count() == 0:
            print("\n\n\n############################")
            print("ERROR: Database, or table, or schema not found in query:\n")
            #print(query)
            print("The program will be end.")
            sys.exit(-1)
            print("############################\n\n\n")

        df_pandas = df.toPandas()
        print('CADARQ', df_pandas)
        return df_pandas

    def insert_cad_tabelas(self, servidor, schema, tabela,
        where, driver, split_by, mappers, fetch_size, id_banco, cast,
        ignore, delta_column, delta_column2, delta_operation):
        """
        Cadastra registros na tabela repo.cad_tabelas

        Parameters
        ----------
        servidor : str
            Sistema dentro do banco de dados
        schema : str
            Schema da tabela na origem
        tabela : str
            Tabela de origem
        where : str
            Clausula WHERE da query executada pelo Sqoop
        split_by : str
            Campo da tabela origem a para ser usado de divisao em sua captura
        mappers : str
            Quantidade de conexoes abertas do Sqoop com o banco de dados
        fetch_size : str
            Parametro usado para captura de arquivos muito grandes, default =1000
        id_banco : str
            ID referente ao banco de dados (Tecnologia), ex: 1 -Oracle, 2 -Postgresql ...
        cast : str
            Campos da tabela origem que nao sao interpretaveis pelo HIVE, setar como String,
            ex: Campo_UUID=String

        Returns
        -------
            Linha de cadastro na tabela repo.cad_tabelas

        """
        
        con = dbConn(env=self.env, dbname=self.dbname, database=self.db).connect_cad_arq_tabelas()
        cur = con.cursor()
        sql = f"""INSERT INTO repo.cad_tabelas(servidor, "schema", tabela, "where",
        driver, "split-by", mappers, "fetch-size", id_banco, "cast",
	    "ignore", delta_column, delta_column2, delta_operation)
        VALUES('{servidor.upper()}', '{schema.upper()}', '{tabela.upper()}',
        {where}, {driver}, {split_by}, {mappers}, {fetch_size}, {id_banco}, {cast}, {ignore},
        {delta_column}, {delta_column2}, {delta_operation});
        """
        #print(sql)
        print("\n\n\n############################")
        print(f"INSERTING DATA IN {self.dbtable}")
        line = f"{servidor.upper()}|{schema.upper()}|{tabela.upper()}|{where}|{driver}|{split_by}|{mappers}|{fetch_size}|{id_banco}|{cast}||{delta_column}|{delta_column2}|{delta_operation}"
        print(line)
        print("############################\n\n\n")
        cur.execute(sql)  
        con.commit()
        con.close()

    def insert_cad_arq(self, nome_arq, periodicidade, destino, database, tabela, incremtal_overwrite):
        """
        Cadastra registros na tabela repo.cad_arq

        Parameters
        ----------
        nome_arq : str
            Nome do arquivo - schema.tabela
        periodicidade : str
            Periodicidade na carga dos dados, D = Diario, M = Mensal
        destino : str
            Path no s3, na camada raw, a ser armazenado
        database : str
            Database da tabela na origem
        tabela : str
            Tabela de origem
        incremtal_overwrite : str
            Definicao para armazenamento do dado, I = Incremental(append) O = Overwrite

        Returns
        -------
            Linha de cadastro na tabela repo.cad_arq

        """
        
        con = dbConn(env=self.env, dbname=self.dbname, database=self.db).connect_cad_arq_tabelas()
        cur = con.cursor()
        sql = f"""INSERT INTO repo.cad_arq(nome_arq, periodicidade, destino,
	    "database", tabela, incremtal_overwrite)
        VALUES('{nome_arq}', '{periodicidade}', '{destino.lower()}',
	    '{database.lower()}', '{tabela.lower()}', '{incremtal_overwrite.upper()}') 
        """
        #print(sql)
        print("\n\n\n############################")
        print(f"INSERTING DATA IN {self.dbtable}")
        line = f"{nome_arq}|{periodicidade}|{destino.lower()}|{database.lower()}|{tabela.lower()}|{incremtal_overwrite.upper()}"
        print(line)
        print("############################\n\n\n")
        cur.execute(sql)        
        con.commit()
        con.close()


#cad_tabelas = CadArqTabelas("dev", "repo.cad_tabelas", "REPOSITORIOINGESTION", "postgresql").get_cad_tabelas_as_df()
#cad_arq = CadArqTabelas("dev", "repo.cad_arq", "REPOSITORIOINGESTION", "postgresql").get_cad_arq_as_df()
