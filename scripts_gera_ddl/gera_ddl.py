import boto3
import pandas as pd
import scripts_gera_ddl.ddl_templates as _template
from scripts_utils.utils import run_create_hive_table


def gera_ddl_jdbc(dbname, servidor, tabela, env: str, partition, layer: str = 'ALL', schema=None):
    """
    Gera arquivo .hql para criacao da tabela hive baseado em banco de dados

    Parameters
    ----------
    dbname : str
        Banco de dados (Tecnologia), ex: Oracle, Postgresql
    servidor : str
        Sistema dentro do banco de dados
    schema : str
        Schema da tabela na origem
    tabela : str
        Tabela de origem
    env : str
        Ambiente (DEV/PRD)
    layer : str
        Camada do Lake, all = RAW e TRUSTED
    partition : str
        Create table with or not partition

    Returns
    -------
        DDL para criacao da tabela HIVE nas camadas por camada
    """
    database = servidor
    owner = schema
    table = tabela
    raw_location = tabela
    layer = layer.upper()

    print("\n\n\n############################")
    print("EXECUTING FUNCTION CREATE_DDL FOR JDBC")
    print("layer = ", layer)
    print("dbname = ", dbname)
    print("database = ", database)
    print("schema = ", owner)
    print("table = ", table)
    print("raw_location = ", raw_location)
    print("############################\n\n\n")

    client = boto3.resource('s3')

    if layer.upper() == 'RAW' or layer.upper() == 'ALL':
        ddl = _template.ddl_template_raw(dbname, database, owner, table, raw_location, env=env, partition=partition)
        # with open(f'/home/hadoop/ddl_hive/{env}-RAW-{database}.{table}.hql', 'w') as file:
        #     file.write(ddl)
        # Inserido essa parte de baixo devido a permission denied na pasta hadoop
        client = boto3.resource("s3")
        client.Bucket(f"natura-datalake-{env}").put_object(
            Body=ddl, Key=f'pre_ingestion/ddl_hive/{env}-RAW-{database}.{table}.hql')
        run_create_hive_table(ddl, layer='RAW', table=table)

    if layer.upper() == 'TRUSTED' or layer.upper() == 'ALL':
        #if schema.lower() != 'sisdmpm':
        # if database.lower() != 'o01dw':
        if database.lower() not in ['o01dw', 'o01dwh']:
            ddl = _template.ddl_template_trusted(dbname, database, owner, table, raw_location, env=env, partition=partition)
            # with open(f'/home/hadoop/ddl_hive/{env}-TRUSTED-{database}.{table}.hql', 'w') as file:
            #     file.write(ddl)
            # Inserido essa parte de baixo devido a permission denied na pasta hadoop
            client = boto3.resource("s3")
            client.Bucket(f"natura-datalake-{env}").put_object(
                Body=ddl, Key=f'pre_ingestion/ddl_hive/{env}-TRUSTED-{database}.{table}.hql')
            run_create_hive_table(ddl, layer='TRUSTED', table=table)
        else:
            ddl = _template.ddl_template_bau(dbname, database, owner, table, raw_location, env=env, partition=partition)
            # with open(f'/home/hadoop/ddl_hive/{env}-bau-{database}.{table}.hql', 'w') as file:
            #     file.write(ddl)
            # Inserido essa parte de baixo devido a permission denied na pasta hadoop
            client = boto3.resource("s3")
            client.Bucket(f"natura-datalake-{env}").put_object(
                Body=ddl, Key=f'pre_ingestion/ddl_hive/{env}-BAU-{database}.{table}.hql')
            run_create_hive_table(ddl, layer='BAU', table=table)
    

def gera_ddl_file(tabela, file_path, delimiter, env: str, layer: str = 'ALL', ref_dtype: str = 'oracle'):
    """
    Gera arquivo .hql para criacao da tabela hive baseado em arquivo

    Parameters
    ----------
    tabela : str
        Tabela de origem(Nomenclatura a ser utilizada no Lake)
    file_path : str
        Sistema dentro do banco de dados
    delimiter : str
        Schema da tabela na origem
    env : str
        Ambiente (DEV/PRD)
    layer : str
        Camada do Lake, all = RAW e TRUSTED

    Returns
    -------
        DDL para criacao da tabela HIVE nas camadas por camada

    """
    print("\n\n\n############################")
    print("EXECUTING FUNCTION CREATE_DDL FOR FILE")
    print("LAYER = ", layer)
    print("############################\n\n\n")
    raw_location = tabela
    table = tabela

    client = boto3.resource('s3')

    if layer.upper() == 'RAW' or layer.upper() == 'ALL':
        ddl = _template.ddl_template_file_raw(file_path, table, raw_location, env, delimiter)
        # with open(f'/home/hadoop/ddl_hive/{env.lower()}-RAW-{table.lower()}.hql', 'w') as file:
        #     file.write(ddl)
        # Inserido essa parte de baixo devido a permission denied na pasta hadoop
        client = boto3.resource("s3")
        client.Bucket(f"natura-datalake-{env}").put_object(
            Body=ddl, Key=f'pre_ingestion/ddl_hive/{env.lower()}-RAW-{table.lower()}.hql')
        run_create_hive_table(ddl, layer='RAW', table=table)

    if layer.upper() == 'TRUSTED' or layer.upper() == 'ALL':
        ddl = _template.ddl_template_file_trusted(file_path, table, raw_location, env=env, ref_dtype=ref_dtype)
        # with open(f'/home/hadoop/ddl_hive/{env.lower()}-TRUSTED-{table.lower()}.hql', 'w') as file:
        #     file.write(ddl)
        # Inserido essa parte de baixo devido a permission denied na pasta hadoop
        client = boto3.resource("s3")
        client.Bucket(f"natura-datalake-{env}").put_object(
            Body=ddl, Key=f'pre_ingestion/ddl_hive/{env.lower()}-TRUSTED-{table.lower()}.hql')
        run_create_hive_table(ddl, layer='TRUSTED', table=table)
