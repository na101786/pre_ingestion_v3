#
# Módulo de tipos de-para
#
# Author: Gabriel R. Pereira <gabrielrichard@natura.net> e Vanessa C. Scheffer <vanessaschefferd@natura.net>
# Version: 2.0
#


def dtype_lookup_oracle(data_type, data_precision, data_scale):
    """
    Busca o tipo Hive equivalente ao Oracle.

    Tabela de busca para conversão de tipos entre bancos de dados Oracle e Hive.

    Parameters
    ----------
    data_type : str
        Tipo do dado, conforme metadados do Oracle
    data_precision : str
        Precisão do tipo, conforme metadados do Oracle
    data_scale : str
        Escala do tipo, conforme metadados do Oracle

    Returns
    -------
    str
        Nome do tipo equivalente no Hive.
        Se o tipo não foi identificado, retorna 'STRING'.
    """

    if data_type == 'NUMBER':
        import math

        if data_scale == 0 or data_scale is None or (isinstance(data_scale, float) and math.isnan(data_scale)):
            if data_precision is None or (isinstance(data_precision, float) and math.isnan(data_precision)):
                return 'BIGINT'
            elif data_precision <= 2:
                return 'TINYINT'
            elif data_precision <= 4:
                return 'SMALLINT'
            elif data_precision <= 9:
                return 'INT'
            else:
                return 'BIGINT'
        else:
            return f'DECIMAL({int(data_precision)},{int(data_scale)})'

    elif data_type in ['VARCHAR2', 'CHAR', 'DATE', 'TIMESTAMP']:
        return 'STRING'
    else:
        return 'STRING'


def dtype_lookup_postgres(data_type, data_precision, data_scale):
    """
    Busca o tipo Hive equivalente ao Postgres.

    Tabela de busca para conversão de tipos entre bancos de dados Oracle e Hive.

    Parameters
    ----------
    data_type : str
        Tipo do dado, conforme metadados do Oracle
    data_precision : str
        Precisão do tipo, conforme metadados do Oracle
    data_scale : str
        Escala do tipo, conforme metadados do Oracle

    Returns
    -------
    str
        Nome do tipo equivalente no Hive.
        Se o tipo não foi identificado, retorna 'STRING'.
    """
    # https://www.postgresql.org/docs/9.5/datatype.html
    # https://cwiki.apache.org/confluence/display/Hive/LanguageManual+Types#LanguageManualTypes-NumericTypes
    if data_type in ['NUMERIC', 'DECIMAL']:
        import math

        if data_scale == 0 or data_scale is None or (isinstance(data_scale, float) and math.isnan(data_scale)):
            if data_precision is None or (isinstance(data_precision, float) and math.isnan(data_precision)):
                return 'BIGINT'
            elif data_precision <= 2:
                return 'TINYINT'
            elif data_precision <= 4:
                return 'SMALLINT'
            elif data_precision <= 9:
                return 'INT'
            else:
                return 'BIGINT'
    elif data_type in ['INTEGER', 'INT', 'INT4']:
        return 'INT'
    elif data_type in ['BIGINT', 'SMALLINT', 'INT8', 'INT2']:
        return data_type
    elif data_type in ['DOUBLE PRECISION', 'FLOAT8']:
        return 'DOUBLE'
    elif data_type in ['REAL', 'FLOAT4']:
        return 'FLOAT'
    else:
        return 'STRING'
        

##############################################################
def dtype_lookup_sqlserver(data_type, data_precision, data_scale):
    """
    Busca o tipo Hive equivalente ao SQL Server.

    Tabela de busca para conversão de tipos entre bancos de dados Oracle e Hive.

    Parameters
    ----------
    data_type : str
        Tipo do dado, conforme metadados do sqlserver
    data_precision : str
        Precisão do tipo, conforme metadados do sqlserver
    data_scale : str
        Escala do tipo, conforme metadados do sqlserver

    Returns
    -------
    str
        Nome do tipo equivalente no Hive.
        Se o tipo não foi identificado, retorna 'STRING'.
    """
    
    # https://cwiki.apache.org/confluence/display/Hive/LanguageManual+Types#LanguageManualTypes-NumericTypes
    if data_type.upper() in ['INT']:
        return data_type
    elif data_type.upper() in ['BIGINT', 'SMALLINT', 'INT8', 'INT2']:
        return data_type
    elif data_type.upper() in ['DOUBLE PRECISION', 'FLOAT8']:
        return 'DOUBLE'
    elif data_type.upper() in ['REAL', 'FLOAT4']:
        return 'FLOAT'
    else:
        return 'STRING'

def dtype_lookup_saphana(data_type, data_scale):
    """
    Busca o tipo Hive equivalente ao SAP HANA.

    Tabela de busca para conversão de tipos entre bancos de dados SAP HANA e Hive.

    Parameters
    ----------
    data_type : str
        Tipo do dado, conforme metadados do SAP HANA
    data_precision : str
        Precisão do tipo, conforme metadados do SAP HANA
    data_scale : str
        Escala do tipo, conforme metadados do SAP HANA

    Returns
    -------
    str
        Nome do tipo equivalente no Hive.
        Se o tipo não foi identificado, retorna 'STRING'.
    """

    if data_type in ['TINYINT', 'SMALLINT', 'INTEGER', 'BIGINT', 'DECIMAL', 'DOUBLE', 'BOOLEAN']:
        return data_type

    elif data_type == "REAL":
        return "FLOAT"

    elif data_type == "VARBINARY":
        return "BINARY"

    else:
        return 'STRING'

def dtype_lookup_mysql(data_type):
    """
    Busca o tipo Hive equivalente ao MySQL.

    Tabela de busca para conversão de tipos entre bancos de dados MySQL e Hive.

    Parameters
    ----------
    data_type : str
        Tipo do dado, conforme metadados do MySQL
    data_precision : str
        Precisão do tipo, conforme metadados do MySQL
    data_scale : str
        Escala do tipo, conforme metadados do MySQL

    Returns
    -------
    str
        Nome do tipo equivalente no Hive.
        Se o tipo não foi identificado, retorna 'STRING'.
    """
    
    if data_type in ['TINYINT', 'SMALLINT', 'INTEGER', 'BIGINT', 'DECIMAL', 'DOUBLE', 'BOOLEAN']:
        return data_type

    elif data_type == "MEDIUMINT":
        return "INT"

    elif data_type == "VARBINARY":
        return "BINARY"

    else:
        return 'STRING'

