#
# Módulo de 
#
# Author: Gabriel R. Pereira <gabrielrichard@natura.net> e Vanessa C. Scheffer <vanessaschefferd@natura.net>
# Version: 2.0
# 

import os, sys

import pandas as pd
from pyspark.sql import SparkSession

from db_conexao.dbconn import dbConn
from scripts_gera_ddl.dtype_lookups import dtype_lookup_oracle, dtype_lookup_postgres, dtype_lookup_saphana, dtype_lookup_mysql
from scripts_gera_ddl.dtype_lookups import dtype_lookup_sqlserver


def get_bd_fields_as_df(query, dbname: str, db: str, env: str):
    if dbname.lower() == 'oracle':
        conn = dbConn(env=env, dbname='oracle', database=db).connect_oracle()
    elif dbname.lower() == 'postgresql' or dbname.lower() == 'postgres':
        conn = dbConn(env=env, dbname='postgresql', database=db).connect_postgresql()
    elif dbname.lower() == 'saphana' or dbname.lower() == 'saphana':
        conn = dbConn(env=env, dbname='saphana', database=db).connect_saphana()
    elif dbname.lower() == 'mysql' or dbname.lower() == 'mysql':
        conn = dbConn(env=env, dbname='mysql', database=db).connect_mysql()
    # elif dbname.lower() == 'sqlserver':
    #     conn = dbConn(env=env, dbname='sqlserver', database=db).connect_sqlserver()

    df = pd.read_sql(query, con=conn)
    if df.empty:
        print("\n\n\n############################")
        print("ERROR: Database, or table, or schema not found in query:\n")
        print(query)
        print("The program will be end.")
        sys.exit(-1)
        print("############################\n\n\n")

    df.columns = df.columns.str.upper()
    df['COLUMN_NAME'] = df['COLUMN_NAME'].str.upper()

    if dbname.lower() == 'saphana' or dbname.lower() == 'saphana':
        df['DATA_TYPE_NAME'] = df['DATA_TYPE_NAME'].str.upper()
    else:
        df['DATA_TYPE'] = df['DATA_TYPE'].str.upper()
    print("\n\n###################### DATAFRAME RESULTED:")
    print(df)
    conn.close()
    return df


def get_bd_fields_as_df_spark(query, dbname: str, db: str, env: str):
    spark = SparkSession.builder.getOrCreate()
    spark.sparkContext.setLogLevel('ERROR')

    paramns = dbConn(env=env, dbname=dbname, database=db).get_sqlserver_connection_params()

    jdbc_url = paramns['jdbc_url']
    user = paramns['username']
    password = paramns['password']

    df = spark.read.format("jdbc") \
        .option("user", user) \
        .option("password", password) \
        .option("url", jdbc_url) \
        .option("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver") \
        .option("query", query).load()

    if df.count() == 0:
        print("\n\n\n############################")
        print("ERROR: Database, or table, or schema not found in query:\n")
        print(query)
        print("The program will be end.")
        sys.exit(-1)
        print("############################\n\n\n")

    df_pandas = df.toPandas()
    df_pandas.columns = df_pandas.columns.str.upper()
    df_pandas['COLUMN_NAME'] = df_pandas['COLUMN_NAME'].str.upper()
    df_pandas['DATA_TYPE'] = df_pandas['DATA_TYPE'].str.upper()
    print("\n\n###################### DATAFRAME RESULTED:")
    print(df_pandas)
    return df_pandas


def get_fields_dtypes_file(file_path: str, table: str, env: str, ref_dtype: str, forced_dtype: str = None) -> tuple:
    """
    Retorna os nomes dos campos de um arquivo com seus respectivos tipos no Hive.
    Retorna quantas linhas devem ser ignoradas na camada raw.
    Retorna qual coluna deve ser ignorada na camada trusted.

    Baseado nos metadados do banco de dados Oracle de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    file_path : str
        Caminho do arquivo com os metadados no S3.
    table : str
        Nome do arquivo de origem. Será usado para criar a tabela.
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    tuple
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço.
        Quantidade de linhas devem ser ignoradas na camada raw.
        Coluna deve ser ignorada na camada trusted.
    """
    source_path = f"{file_path}{table}.csv"
    df = pd.read_csv(source_path, sep="|")
    df = df.fillna("")
    df['TABLE_NAME'] = df['TABLE_NAME'].str.upper()
    df['COLUMN_NAME'] = df['COLUMN_NAME'].str.upper()
    df['DATA_TYPE'] = df['DATA_TYPE'].str.upper()

    fields = []
    nlines = df.shape[0]
    for i in range(nlines):
        dt, dp, ds = df.loc[i]['DATA_TYPE'], df.loc[i]['DATA_PRECISION'], df.loc[i]['DATA_SCALE']
        if ref_dtype == 'oracle':
            dtype = dtype_lookup_oracle(dt, dp, ds)
        elif ref_dtype == 'postgresql':
            dtype = dtype_lookup_postgres(dt, dp, ds)
        else:
            dtype = forced_dtype
        fields.append(f"{df.loc[i]['COLUMN_NAME']} {dtype}")

    try:
        skip_header = df['SKIP_HEADER'].values[0]
    except KeyError:
        skip_header = df['SKIP_ROW'].values[0]
    try:
        skip_footer = df['SKIP_FOOTER'].values[0]
    except KeyError:
        skip_footer = 0

    skip_column = df['SKIP_COLUMN'].values[0]

    return fields, skip_header, skip_footer, skip_column

def get_fields_dtypes_oracle(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    """
    Retorna os nomes dos campos da tabela e seus respectivos tipos no Hive

    Baseado nos metadados do banco de dados Oracle de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    db : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Nome da tabela de origem
    env : str
        Ambiente de execução
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    str
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço
        em branco.
    """
    owner = owner.upper()
    table = table.upper()
    if owner in table:
        table = table.replace(owner+'_', "")
    df = get_bd_fields_as_df(
        f"""
        SELECT 
            COLUMN_NAME, 
            DATA_TYPE,
            DATA_PRECISION, 
            DATA_SCALE 
        FROM all_tab_columns 
        WHERE TABLE_NAME='{table}' AND OWNER='{owner}' 
        ORDER BY COLUMN_ID ASC
        """,
        "oracle",
        db,
        env
    )

    fields = []
    nlines = df.shape[0]
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_oracle(
                df.loc[i]['DATA_TYPE'],
                df.loc[i]['DATA_PRECISION'],
                df.loc[i]['DATA_SCALE']
            )

        fields.append(f"`{df.loc[i]['COLUMN_NAME']}` {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)

    return fields_dtype


def get_fields_dtypes_postgres(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    """
    Retorna os nomes dos campos da tabela e seus respectivos tipos no Hive

    Baseado nos metadados do banco de dados Postgres de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    db : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Nome da tabela de origem
    env : str
        Ambiente de execução
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    str
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço
        em branco.
    """
    owner = owner.lower()
    table = table.lower()
    if owner in table:
        table = table.replace(owner+'_', "")
    df = get_bd_fields_as_df(
        f"""
        SELECT 
            COLUMN_NAME, 
            DATA_TYPE,
            NUMERIC_PRECISION, 
            NUMERIC_SCALE 
        FROM information_schema.columns
        WHERE table_schema='{owner}'and table_name='{table}' 
        ORDER BY ordinal_position ASC
        """,
        "postgresql",
        db,
        env
    )

    fields = []
    nlines = df.shape[0]
    print("\n\n######################")
    print(nlines)
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_postgres(
                df.loc[i]['DATA_TYPE'],
                df.loc[i]['NUMERIC_PRECISION'],
                df.loc[i]['NUMERIC_SCALE']
            )

        fields.append(f"`{df.loc[i]['COLUMN_NAME']}` {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)

    return fields_dtype


def get_fields_dtypes_sqlserver(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    owner = owner.lower()
    table = table.lower()
    if owner in table:
        table = table.replace(owner+'_', "")

    df_pandas = get_bd_fields_as_df_spark(
        f"""
            SELECT 
                COLUMN_NAME, 
                DATA_TYPE,
                NUMERIC_PRECISION, 
                NUMERIC_SCALE 
            FROM information_schema.columns
            WHERE table_schema='{owner}'and table_name='{table}' 
            """,
        "sqlserver",
        db,
        env
    )
    ##################### Spark

    # fields = []
    # #nlines = df.shape[0]
    # nlines = df.count()
    # rows_collection = df.collect()
    # print("\n\n######################")
    # print(nlines)
    # for i in range(nlines):
    #     if forced_dtype:
    #         dtype = forced_dtype
    #     else:
    #         dtype = dtype_lookup_sqlserver(
    #             rows_collection[i]['DATA_TYPE'],
    #             rows_collection[i]['NUMERIC_PRECISION'],
    #             rows_collection[i]['NUMERIC_SCALE']
    #         )
    #
    #     fields.append(f"{df.collect()[i]['COLUMN_NAME']} {dtype}")


    ############### Pandas
    fields = []
    nlines = df_pandas.shape[0]
    print("\n\n######################")
    print(nlines)
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_sqlserver(
                df_pandas.loc[i]['DATA_TYPE'],
                df_pandas.loc[i]['NUMERIC_PRECISION'],
                df_pandas.loc[i]['NUMERIC_SCALE']
            )

        fields.append(f"`{df_pandas.loc[i]['COLUMN_NAME']}` {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)
    return fields_dtype
    

def get_fields_dtypes_saphana(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    """
    Retorna os nomes dos campos da tabela e seus respectivos tipos no Hive

    Baseado nos metadados do banco de dados Sap_Hana de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    db : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Nome da tabela de origem
    env : str
        Ambiente de execução
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    str
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço
        em branco.
    """
    owner = owner.upper()
    table = table.upper()
    if owner in table:
        table = table.replace(owner+'_', "")
    df = get_bd_fields_as_df(
        f"""
        SELECT 
            COLUMN_NAME, 
            DATA_TYPE_NAME, 
            SCALE 
        FROM TABLE_COLUMNS 
        WHERE TABLE_NAME='{table}' AND SCHEMA_NAME='{owner}' 
        ORDER BY COLUMN_ID ASC
        """,
        "saphana",
        db,
        env
    )

    fields = []
    nlines = df.shape[0]
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_saphana(
                df.loc[i]['DATA_TYPE_NAME'],
                df.loc[i]['SCALE']
            )

        fields.append(f"`{df.loc[i]['COLUMN_NAME']}` {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)

    return fields_dtype

def get_fields_dtypes_mysql(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    """
    Retorna os nomes dos campos da tabela e seus respectivos tipos no Hive

    Baseado nos metadados do banco de dados MySQL de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    db : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Nome da tabela de origem
    env : str
        Ambiente de execução
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    str
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço
        em branco.
    """
    table = table.upper()
    df = get_bd_fields_as_df(
        f"""
        SELECT
            COLUMN_NAME,
            DATA_TYPE,
            NUMERIC_PRECISION,
            NUMERIC_SCALE
        FROM information_schema.columns
        WHERE table_name='{table}'
        AND table_schema='{owner}'
        ORDER BY ordinal_position ASC
        """,
        "mysql",
        db,
        env
    )

    fields = []
    nlines = df.shape[0]
    print("\n\n######################")
    print(nlines)
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_mysql(
                df.loc[i]['DATA_TYPE']
            )

        fields.append(f"`{df.loc[i]['COLUMN_NAME']}` {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)

    return fields_dtype