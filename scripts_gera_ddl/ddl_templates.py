#
# Módulo de 
#
# Author: Gabriel R. Pereira <gabrielrichard@natura.net> e Vanessa C. Scheffer <vanessaschefferd@natura.net>
# Version: 2.0
# 

import os
import math
from scripts_gera_ddl.get_fields_dtypes import get_fields_dtypes_oracle, get_fields_dtypes_postgres, get_fields_dtypes_file, get_fields_dtypes_saphana, get_fields_dtypes_mysql
from scripts_gera_ddl.get_fields_dtypes import get_fields_dtypes_sqlserver


def ddl_template_raw(dbname: str, database: str, owner: str, table: str, location: str, env: str, partition) -> str:
    """
    Retorna o template de uma DDL para a camada RAW do Datalake

    Parameters
    ----------
    dbname : str
        Database name
    database : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Tabela de origem
    location : str
        Localização do arquivo da tabela dentro da camada RAW, no bucket do Datalake no S3
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DDL para geração da tabela no Hive
    """
    print(f"\n\n\ndbname = {dbname}")
    if dbname.upper() == 'ORACLE':
        print("\n\n###################### INVOKING FUNCTION --> GET FIELDS DTYPES ORACLE")
        fields_dtype = get_fields_dtypes_oracle(database, owner, table, env, forced_dtype='STRING')
    elif dbname.upper() == 'POSTGRESQL':
        print("\n\n###################### INVOKING FUNCTION --> GET FIELDS DTYPES POSTGRES")
        fields_dtype = get_fields_dtypes_postgres(database, owner, table, env, forced_dtype='STRING')
    elif dbname.upper() == 'SQLSERVER':
        print("\n\n###################### INVOKING FUNCTION --> GET FIELDS DTYPES SQLSERVER")
        fields_dtype = get_fields_dtypes_sqlserver(database, owner, table, env, forced_dtype='STRING')
    elif dbname.upper() == 'SAPHANA':
        print("\n\n###################### INVOKING FUNCTION --> GET FIELDS DTYPES SAPHANA")
        fields_dtype = get_fields_dtypes_saphana(database, owner, table, env, forced_dtype='STRING')
    elif dbname.upper() == 'MYSQL':
        print("\n\n###################### INVOKING FUNCTION --> GET FIELDS DTYPES MYSQL")
        fields_dtype = get_fields_dtypes_mysql(database, owner, table, env, forced_dtype='STRING')

    print(fields_dtype)

    env = env.lower()
    location = location.lower()

    table = table.upper()
    database = database.upper()

    # identação estranha, mas intencional
    template1 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS RAW.{table}(
    {fields_dtype}
)
PARTITIONED BY (anomesdia int)
ROW FORMAT delimited
fields terminated by '\\001'
lines terminated by '\\n' stored as textfile
LOCATION 's3://natura-datalake-{env}/raw/{location}';
"""

    template2 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS RAW.{table}(
    {fields_dtype}
)
ROW FORMAT delimited
fields terminated by '\\001'
lines terminated by '\\n' stored as textfile
LOCATION 's3://natura-datalake-{env}/raw/{location}';
"""

    if partition == 'yes':
        return template1
    else:
        return template2


def ddl_template_trusted(dbname: str, database: str, owner: str, table: str, location: str, env: str, partition):
    """
    Retorna o template de uma DDL para a camada TRUSTED do Datalake

    Os tipos do banco de dados Oracle de origem são convertidos para seus equivalentes no Hive.

    Parameters
    ----------
    dbname : str
        Database name
    database : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Tabela de origem
    location : str
        Localização do arquivo da tabela dentro da camada TRUSTED, no bucket do Datalake no S3
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DDL para geração da tabela no Hive
    """
    if dbname.upper() == 'ORACLE':
        fields_dtype = get_fields_dtypes_oracle(database, owner, table, env)
    elif dbname.upper() == 'POSTGRESQL':
        fields_dtype = get_fields_dtypes_postgres(database, owner, table, env)
    elif dbname.upper() == "SQLSERVER":
        fields_dtype = get_fields_dtypes_sqlserver(database, owner, table, env)
    elif dbname.upper() == 'SAPHANA':
        fields_dtype = get_fields_dtypes_saphana(database, owner, table, env)
    elif dbname.upper() == 'MYSQL':
        fields_dtype = get_fields_dtypes_mysql(database, owner, table, env)

    env = env.lower()
    location = location.lower()

    table = table.upper()
    database = database.upper()

    template1 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS TRUSTED.{table}(
    {fields_dtype}
)
PARTITIONED BY (`anomesdia` int)
STORED AS parquet
LOCATION 's3://natura-datalake-{env}/trusted/{location}';
"""

    template2 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS TRUSTED.{table}(
        {fields_dtype}
    )
    STORED AS parquet
    LOCATION 's3://natura-datalake-{env}/trusted/{location}';
    """

    if partition == 'yes':
        return template1
    else:
        return template2


def ddl_template_bau(dbname: str, database: str, owner: str, table: str, location: str, env: str, partition):
    """
    Retorna o template de uma DDL para a camada bau do Datalake

    Os tipos do banco de dados Oracle de origem são convertidos para seus equivalentes no Hive.

    Parameters
    ----------
    dbname : str
        Database name
    database : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Tabela de origem
    location : str
        Localização do arquivo da tabela dentro da camada bau, no bucket do Datalake no S3
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DDL para geração da tabela no Hive
    """
    if dbname.upper() == 'ORACLE':
        fields_dtype = get_fields_dtypes_oracle(database, owner, table, env)
    elif dbname.upper() == 'POSTGRESQL':
        fields_dtype = get_fields_dtypes_postgres(database, owner, table, env)
    elif dbname.upper() == "SQLSERVER":
        fields_dtype = get_fields_dtypes_sqlserver(database, owner, table, env)
    elif dbname.upper() == 'SAPHANA':
        fields_dtype = get_fields_dtypes_saphana(database, owner, table, env)
    elif dbname.upper() == 'MYSQL':
        fields_dtype = get_fields_dtypes_mysql(database, owner, table, env)

    env = env.lower()
    location = location.lower()

    table = table.upper()
    database = database.upper()

    template1 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS BAU.{table}(
        {fields_dtype}
    )
    PARTITIONED BY (`anomesdia` int)
    STORED AS parquet
    LOCATION 's3://natura-datalake-{env}/bau/{location}';
    """

    template2 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS BAU.{table}(
            {fields_dtype}
        )
        STORED AS parquet
        LOCATION 's3://natura-datalake-{env}/bau/{location}';
        """

    if partition == 'yes':
        return template1
    else:
        return template2


def ddl_template_file_raw(file_path: str, table: str, location: str, env: str, delimiter: str) -> str:
    """
    Retorna o template de uma DDL para a camada RAW do Datalake.
    Template 1 = Arquivo sem header E sem footer
    Template 2 = Arquivo com header E com footer
    Template 3 = Arquivo com header E sem footer
    Template 4 = Arquivo sem header E com footer

    Parameters
    ----------
    file_path : str
        Caminho do arquivo com os metadados no S3.
    table : str
        Nome do arquivo de origem. Será usado para criar a tabela.
    location : str
        Localização do arquivo da tabela dentro da camada RAW, no bucket do Datalake no S3
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DDL para geração da tabela no Hive
    """
    fields, skip_header, skip_footer, skip_column = get_fields_dtypes_file(file_path, table, env, ref_dtype=None, forced_dtype='STRING')
    fields_dtype = f', {os.linesep}    '.join(fields)

    env = env.lower()
    location = location.lower()
    table = table.upper()

    # Template 1 = Arquivo sem header E sem footer
    # Template 2 = Arquivo com header E com footer
    # Template 3 = Arquivo com header E sem footer
    # Template 4 = Arquivo sem header E com footer

    # identação estranha, mas intencional
    template_1 = f"""CREATE EXTERNAL TABLE IF NOT EXISTS RAW.{table}(
        {fields_dtype}
)
PARTITIONED BY (anomesdia int)
ROW FORMAT delimited
fields terminated by '{delimiter}'
lines terminated by '\\n' stored as textfile
LOCATION 's3://natura-datalake-{env}/raw/{location}'
"""

    template_2 = template_1 + f'tblproperties ("skip.header.line.count"="{skip_header}",' \
                                             f'"skip.footer.line.count"="{skip_footer}")'
    template_3 = template_1 + f'tblproperties ("skip.header.line.count"="{skip_header}")'
    template_4 = template_1 + f'tblproperties ("skip.footer.line.count"="{skip_footer}")'
    
    if skip_header != 0 and skip_footer != 0:
        return template_2
    elif skip_header != 0 and skip_footer == 0:
        return template_3
    elif skip_header == 0 and skip_footer != 0:
        return template_4

    return template_1
  

def ddl_template_file_trusted(file_path: str, table: str, location: str, env: str, ref_dtype: str) -> str:
    """
    Retorna o template de uma DDL para a camada RAW do Datalake

    Parameters
    ----------
    file_path : str
        Caminho do arquivo com os metadados no S3.
    table : str
        Nome do arquivo de origem. Será usado para criar a tabela.
    location : str
        Localização do arquivo da tabela dentro da camada TRUSTED, no bucket do Datalake no S3
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DDL para geração da tabela no Hive
    """
    if ref_dtype:
        ref_dtype = ref_dtype.lower()
    else:
        ref_dtype = 'oracle'
    
    print("++++++++++++++++++++++++++++++++++++++++++++++++")
    print(ref_dtype)
   
    fields, skip_header, skip_footer, skip_column = get_fields_dtypes_file(file_path, table, env, ref_dtype, forced_dtype=None)
    #skip_column = None if math.isnan(skip_column) else skip_column
    print('fields = ', fields)
    print('skip_column = ', skip_column)
    #if skip_column:
    if skip_column != '':
        skip_column = [item for item in fields if skip_column in item][0]
        fields.remove(skip_column)
    print('fields = ', fields)
    fields_dtype = f', {os.linesep}    '.join(fields)

    env = env.lower()
    location = location.lower()
    table = table.upper()

    # identação estranha, mas intencional
    return f"""CREATE EXTERNAL TABLE IF NOT EXISTS TRUSTED.{table}(
    {fields_dtype}
)
PARTITIONED BY (`anomesdia` int)
STORED AS parquet
LOCATION 's3://natura-datalake-{env}/trusted/{location}'
"""
