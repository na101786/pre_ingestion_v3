#
# Módulo de
#
# Author: Gabriel R. Pereira <gabrielrichard@natura.net> e Vanessa C. Scheffer <vanessaschefferd@natura.net>
# Version: 2.0
#

import os
import pandas as pd
from scripts_utils.dbconn import connect_oracle, connect_postgresql
from scripts_gera_ddl.dtype_lookups import dtype_lookup_oracle, dtype_lookup_postgres


def get_bd_fields_as_df(query, dbname: str, db: str, env: str):
    if dbname.lower() == 'oracle':
        conn = connect_oracle(db, env)
    elif dbname.lower() == 'postgresql' or dbname.lower() == 'postgres':
        conn = connect_postgresql(db, env)

    df = pd.read_sql(query, con=conn)
    df.columns = df.columns.str.upper()
    df['COLUMN_NAME'] = df['COLUMN_NAME'].str.upper()
    df['DATA_TYPE'] = df['DATA_TYPE'].str.upper()
    print("\n\n###################### DATAFRAME RESULTED:")
    print(df)
    conn.close()
    return df


def get_fields_dtypes_oracle(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    """
    Retorna os nomes dos campos da tabela e seus respectivos tipos no Hive

    Baseado nos metadados do banco de dados Oracle de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    db : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Nome da tabela de origem
    env : str
        Ambiente de execução
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    str
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço
        em branco.
    """
    owner = owner.upper()
    table = table.upper()
    df = get_bd_fields_as_df(
        f"""
        SELECT 
            COLUMN_NAME, 
            DATA_TYPE,
            DATA_PRECISION, 
            DATA_SCALE 
        FROM all_tab_columns 
        WHERE TABLE_NAME='{table}' AND OWNER='{owner}' 
        ORDER BY COLUMN_ID ASC
        """,
        "oracle",
        db,
        env
    )

    fields = []
    nlines = df.shape[0]
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_oracle(
                df.loc[i]['DATA_TYPE'],
                df.loc[i]['DATA_PRECISION'],
                df.loc[i]['DATA_SCALE']
            )

        fields.append(f"{df.loc[i]['COLUMN_NAME']} {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)

    return fields_dtype


def get_fields_dtypes_postgres(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    """
    Retorna os nomes dos campos da tabela e seus respectivos tipos no Hive

    Baseado nos metadados do banco de dados Postgres de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    db : str
        Banco de dados de origem
    owner : str
        Owner da tabela de origem
    table : str
        Nome da tabela de origem
    env : str
        Ambiente de execução
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    str
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço
        em branco.
    """
    owner = owner.lower()
    table = table.lower()
    df = get_bd_fields_as_df(
        f"""
        SELECT 
            COLUMN_NAME, 
            DATA_TYPE,
            NUMERIC_PRECISION, 
            NUMERIC_SCALE 
        FROM information_schema.columns
        WHERE table_schema='{owner}'and table_name='{table}' 
        ORDER BY ordinal_position ASC
        """,
        "postgresql",
        db,
        env
    )

    fields = []
    nlines = df.shape[0]
    print("\n\n######################")
    print(nlines)
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_postgres(
                df.loc[i]['DATA_TYPE'],
                df.loc[i]['NUMERIC_PRECISION'],
                df.loc[i]['NUMERIC_SCALE']
            )

        fields.append(f"{df.loc[i]['COLUMN_NAME']} {dtype}")

    fields_dtype = f', {os.linesep}    '.join(fields)

    return fields_dtype


def get_fields_dtypes_sqlserver(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    pass


def get_fields_dtypes_mysql(db: str, owner: str, table: str, env: str, forced_dtype: str = None) -> str:
    pass


def get_fields_dtypes_file(file_path: str, table: str, env: str, forced_dtype: str = None) -> tuple:
    """
    Retorna os nomes dos campos de um arquivo com seus respectivos tipos no Hive.
    Retorna quantas linhas devem ser ignoradas na camada raw.
    Retorna qual coluna deve ser ignorada na camada trusted.

    Baseado nos metadados do banco de dados Oracle de origem, retorna todos os campos
    com seus respectivos tipos no Hive.

    Parameters
    ----------
    file_path : str
        Caminho do arquivo com os metadados no S3.
    table : str
        Nome do arquivo de origem. Será usado para criar a tabela.
    forced_dtype : str
        Tipo a ser utilizado pelo Hive para todas as variáveis.
        Se None, usa-se uma tabela de lookup para definir o tipo adequado.

    Returns
    -------
    tuple
        Nome do campo da tabela e tipo do campo no Hive, separados por um espaço.
        Quantidade de linhas devem ser ignoradas na camada raw.
        Coluna deve ser ignorada na camada trusted.
    """
    source_path = f"{file_path}{table}.csv"
    df = pd.read_csv(source_path, sep="|")
    df['TABLE_NAME'] = df['TABLE_NAME'].str.upper()
    df['COLUMN_NAME'] = df['COLUMN_NAME'].str.upper()
    df['DATA_TYPE'] = df['DATA_TYPE'].str.upper()

    fields = []
    nlines = df.shape[0]
    for i in range(nlines):
        if forced_dtype:
            dtype = forced_dtype
        else:
            dtype = dtype_lookup_oracle(
                df.loc[i]['DATA_TYPE'],
                df.loc[i]['DATA_PRECISION'],
                df.loc[i]['DATA_SCALE']
            )

        fields.append(f"{df.loc[i]['COLUMN_NAME']} {dtype}")
    skip_row = df['SKIP_ROW'].values[0]
    skip_column = df['SKIP_COLUMN'].values[0]

    return (fields, skip_row, skip_column)
