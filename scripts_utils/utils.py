import pandas as pd
from IPython.display import display
import numpy as np
import s3fs, traceback, sys
from datetime import date, datetime
from subprocess import call
from db_conexao.dbconn import dbConn
#from scripts_utils.cad_arq_tabelas import CadArqTabelas
from scripts_cadastro.cad_arq_tabelas import CadArqTabelas

def check_duplicates(df: pd.DataFrame, env: str, s_type: str):
    """
    Verifica duplicidade de tabelas nos arquivos de parametro cad_arq.csv e cad_tabelas.csv

    Parameters
    ---------
    df : DataFrame
        DataFrame com as variaveis coletadas
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DataFrame validado
    """
    print("\n\n\n############################")
    print("CHECKING DUPLICATED DATAS")
    print("############################\n")
    cad_arq = CadArqTabelas(env, "repo.cad_arq", "REPOSITORIOINGESTION", "postgresql").get_cad_arq_as_df()
    cad_tabelas = CadArqTabelas(env, "repo.cad_tabelas", "REPOSITORIOINGESTION", "postgresql").get_cad_tabelas_as_df()

    cad_arq = cad_arq.apply(lambda x: x.str.lower()) # transforma as palavras para minúsculo
    cad_tabelas = cad_tabelas.apply(lambda x: x.str.lower()) # transforma as palavras para minúsculo

    #######################################
    # Cria coluna para checkar se já existe
    #######################################
    cad_tabelas['check'] = cad_tabelas['schema']+'.'+cad_tabelas['tabela'] #SISCAD.T_CICLO_OPERACIONAL
    cad_arq['check'] = cad_arq['nome_arq'] # file.dre_planvd

    check_values_1 = cad_tabelas['check'].values # var com values da coluna check
    check_values_2 = cad_arq['check'].values # var com values da coluna check

    check_values = list(set(check_values_1).intersection(set(check_values_2))) # Return a set that contains the
                                                                               # items that exist in both set x, and set y:
                                                                               # Substituir por um Join

    if s_type == 'jdbc':
        df['check'] = df['schema'] + '.' + df['tabela']
    elif s_type == 'file':
        df['check'] = 'file.' + df['file']

    df['check'] = df['check'].apply(lambda x: x.lower())
    df['check'] = df['check'].apply(lambda x: x in check_values)
    #######################################
    # Fim do Cria coluna para checkar se já existe
    #######################################

    if df.loc[df.check == True].shape[0] > 0:
        print("\n\n\n############################")
        print("The follows rows were found in repo.cad_tabelas or repo.cad_arq!")
        print(df.loc[df.check == True][['dbname', 'servidor', 'schema', 'tabela']])
        print("############################\n\n\n")
        print("ERROR: The excel file has rows that are already registered in repo.cad_tabelas or repo.cad_arq")
        print("Check with the data engineer team.")
        print("############################\n\n\n")
        sys.exit(-1)
    else:
        print("\n\n\n############################")
        print(f"You don't have duplicated entries.")


def import_content(env: str) -> pd.DataFrame:
    """
    Importa o conteudo do excel de cadastro para novas tabelas

    Parameters
    ---------
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DataFrame com todos os parametros coletados no excel

    """
    print("\n\n\n############################")
    print("LOADING DATA FROM /pre_ingestion/register_ingestion.xlsm")
    print("############################\n\n\n")
    env = env.lower()
    df = pd.read_excel(f's3://natura-datalake-{env}/pre_ingestion/register_ingestion.xlsm', sheet_name="register", dtype=str)

    # Padroniza o nome das colunas e os dados para minúsculo
    df.columns = df.columns.str.lower()
    df = df.apply(lambda x: x.str.lower())

    # remove linhas duplicadas na planilha register_ingestion.xlsx: schema e tabela
    df = df.drop_duplicates(subset=['schema', 'tabela'])

    # check dados já cadastrados no cad_arq e cad_tabelas: schema e tabela. Se já existir, o processo encerra
    check_duplicates(df.copy(), env, 'jdbc')
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)

    print(' ### DF ###')
    display(df)

    # Se não houver dados já cadastrados, então o processo continua
    # Preenche os campos vazios com string vazia
    df = df.fillna('null')
    #df = df.fillna(np.nan).replace([np.nan], [None])

    # Mostra o que será adicionado
    print("\n############################")
    print("NUMBER OF ROWS TO BE INSERTED AFTER DROP DUPLICATED = ", df.shape[0])
    print(df.columns)
    print(df)
    print("############################\n\n\n")
    if df.shape[0] == 0:
        print("\n############################")
        print("ERROR: There aren't new lines to be inserted. The program will be closed!")
        print("############################\n\n\n")
        sys.exit(-1)

    # Retorna um DataFrame pandas com os registros a serem cadastrados.
    return df


def import_content_file(env: str) -> pd.DataFrame:
    """
    Importa o conteudo do excel de cadastro para novas tabelas

    Parameters
    ---------
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        DataFrame com todos os parametros coletados no excel

    """
    print("\n\n\n############################")
    print("LOADING DATA FROM /pre_ingestion/register_ingestion.xlsm")
    print("############################\n\n\n")
    env = env.lower()
    df = pd.read_excel(f's3://natura-datalake-{env}/pre_ingestion/register_ingestion.xlsm',
                       sheet_name="register_file", dtype=str)

    df.columns = df.columns.str.lower()

    # remove duplicações no arquivo carregado: schema e tabela
    df = df.drop_duplicates(subset=['file'])
    df = df.apply(lambda x: x.str.strip())

    # remove duplicações que já existem no cad_arq e cad_tabelas: schema e tabela
    check_duplicates(df.copy(), env, 'file')

    df = df.fillna('null')
    print("\n############################")
    print("NUMBER OF ROWS TO BE INSERTED AFTER DROP DUPLICATED = ", df.shape[0])
    print(df.columns)
    print(df)
    print("############################\n\n\n")
    if df.shape[0] == 0:
        print("\n############################")
        print("ERROR: There aren't new lines to be inserted. The program will be closed!")
        print("############################\n\n\n")
        sys.exit(-1)
    return df


def create_log(df_line: pd.Series, env: str):
    ref1 = date.today().strftime("%Y%m%d")
    ref2 = datetime.today().strftime("%Y-%m-%d_%H:%M:%S")
    s3 = s3fs.S3FileSystem()
    """
    path = f's3://natura-datalake-{env}/pre_ingestion/{ref1}/'
    if not s3.isdir(path):
        s3.mkdir(path)
    df.to_csv(f'{path}ingestion_{ref2}.csv', sep='|')
    """
    print("\n\n\n############################")
    print("INSERTING IN LOG...")
    print("############################\n\n\n")
    df_line['ref'] = ref1
    df_line = df_line.fillna("")
    line = '|'.join(df_line)
    file = s3.open(f's3://natura-datalake-{env}/pre_ingestion/LOG.csv', 'a')
    file.write(line + '\n')
    file.close()


def create_log_file(df_line: pd.Series, env: str):
    ref1 = date.today().strftime("%Y%m%d")
    s3 = s3fs.S3FileSystem()

    print("\n\n\n############################")
    print("INSERTING IN LOG...")
    print("############################\n\n\n")
    df_line['ref'] = ref1
    df_line = df_line.fillna("")
    line = '|'.join(df_line)
    file = s3.open(f's3://natura-datalake-{env}/pre_ingestion/LOG_FILE.csv', 'a')
    file.write(line + '\n')
    file.close()


def run_create_hive_table(ddl: str, layer: str, table: str) -> None:
    """
    Executa o .hql no hive

    Parameters
    ---------
    ddl : str
        Script de criacao da tabela (.HQL)
    layer : str
        Camada do Lake, all = RAW e TRUSTED
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        Criacao da tabela hive

    """
    print("\n\n\n############################")
    print("CREATING HIVE TABLE...")
    print("############################\n\n\n")
    #ddl_drop = f"drop table if exists {layer}.{table}"
    #call(['hive', '-e', ddl_drop])
    try:
        call(['hive', '-e', ddl])
        print(f'\n##### Successfully create hive table {layer}.{table} with ddl: #####\n')
        print(f'{ddl}\n')
    except Exception:
        traceback.print_exc()
        sys.exit(1)


def move_processed(env: str):
    """
    Move a planilha de parametro apos o processamento
    
    """
    dateTimeObj = datetime.now()
    timestampStr = dateTimeObj.strftime("%d-%m-%Y-%H%M%S")
    s3 = s3fs.S3FileSystem()
    s3.mv(f"s3://natura-datalake-{env}/pre_ingestion/register_ingestion.xlsm", f"s3://natura-datalake-{env}/pre_ingestion/processed/register_ingestion-{timestampStr}.xlsm")
