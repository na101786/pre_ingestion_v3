import cx_Oracle, psycopg2, s3fs
import yaml, pandas


def _get_connection_params(dbname: str, database: str, env: str) -> tuple:
    """
    Busca os parametros de conexao com o banco de dados origem

    Parameters
    ----------
    dbname : str
        Banco de dados (Tecnologia), ex: Oracle, Postgresql
    database : str
        Database da tabela na origem
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        Parametros de conexao com o banco de dados

    """
    dbname = dbname.capitalize()
    database = database.upper()
    env = env.lower()
    s3 = s3fs.S3FileSystem()
    credentials_file = s3.open(f"s3://natura-datalake-{env}/sqoop/SqoopConnect{dbname}{database}.txt", mode="r")
    linhas = credentials_file.readlines()
    credentials_file.close()

    for i, linha in enumerate(linhas):
        if '--connect' in linha:
            connect = linhas[i + 1][:-1]
            connect = connect.strip()
            connect = connect.split('//')[1]
            host = connect.split(':')[0]
            port = connect.split(':')[1].split('/')[0]
            bd = connect.split('/')[-1]
        if '--username' in linha:
            username = linhas[i + 1][:-1]
            username = username.strip()
        if '--password' in linha:
            password = linhas[i + 1][:-1]
            password = password.strip()
    return (host, port, bd, username, password)


def connect_oracle(database: str, env: str) -> cx_Oracle.connect:
    """
    Monta a string de conexao com o banco de dados Oracle

    Parameters
    ----------
    database : str
        Database da tabela na origem
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        Parametros de conexao com o banco de dados Oracle

    """
    host, port, database, username, password = _get_connection_params('oracle', database, env)
    conn_str = fr"{username}/{password}@{host}:{port}/{database}"
    return cx_Oracle.connect(conn_str)


def connect_postgresql(database: str, env: str):
    """
    Monta a string de conexao com o banco de dados Postgresql

    Parameters
    ----------
    database : str
        Database da tabela na origem
    env : str
        Ambiente (DEV/PRD)

    Returns
    -------
        Parametros de conexao com o banco de dados Postgresql

    """
    host, port, database, username, password = _get_connection_params('postgresql', database, env)
    conn_str = fr"dbname='{database}' user='{username}' host='{host}' password='{password}' port='{port}'"
    print(conn_str)
    return psycopg2.connect(conn_str)



